//
//  SecondOutputViewController.m
//  delegdin
//
//  Created by APPLE on 26/01/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "SecondOutputViewController.h"
#import "ImageTableViewCell.h"

@interface SecondOutputViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic)NSArray *dataArray;
@property (strong, nonatomic)NSArray *ageArray;

@end

@implementation SecondOutputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // self.scrollView.contentSize = CGSizeMake(500, 800);
    self.dataArray = @[@"din",@"raj",@"kumar"];
    self.ageArray = @[@(23),@(25),@(26)];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"imageCell" forIndexPath:indexPath];
    
    //cell.firstLabel.text = [NSString stringWithFormat:@"dinesh %ld",indexPath.row];
    NSNumber *age =self.ageArray[indexPath.row];
    cell.secondLabel.text = age.stringValue;
    NSString *name = self.dataArray[indexPath.row];
     cell.firstLabel.text = name;
    if ((indexPath.row %2)==0) {
        cell.cellImageView.image = [UIImage imageNamed:@"d"];
    }
    else
    {
        cell.cellImageView.image = [UIImage imageNamed:@"S"];

    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}


- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
if(section == 0)
    {
        return @"din";
    }
    else if(section ==1)
    {
        return @"raj";
    }
    else
    {
        return @"";
    }
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
 return @"don";
}

#pragma mark -TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 200;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 100;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;

{
    UIImageView *imageView = [[UIImageView alloc] initWithImage: [UIImage imageNamed :@"h"]];
    imageView.contentMode =  UIViewContentModeScaleAspectFit;
    return imageView;
}
@end
