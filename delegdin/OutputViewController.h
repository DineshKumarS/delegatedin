//
//  OutputViewController.h
//  delegdin
//
//  Created by APPLE on 25/01/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OutputViewControllerDelegate <NSObject>

-(void)textfied:(NSString *)string;


@end

@protocol OutputViewControllerDataSource <NSObject>

- (NSString *)label;

@end
@interface OutputViewController : UIViewController
@property (nonatomic,weak) id<OutputViewControllerDelegate>delegate;
@property (nonatomic,weak) id<OutputViewControllerDataSource>dataSource;
@end
