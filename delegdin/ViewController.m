//
//  ViewController.m
//  delegdin
//
//  Created by APPLE on 25/01/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "ViewController.h"
#import "OutputViewController.h"
@interface ViewController ()<OutputViewControllerDelegate,OutputViewControllerDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *questionTextField;
@property (weak, nonatomic) IBOutlet UILabel *inputLabel;
@property (weak, nonatomic) IBOutlet UITextField *questionTextField2;
@property (weak, nonatomic) IBOutlet UITextField *questionTextField3;
@property (weak, nonatomic) IBOutlet UITextField *questionTextField4;
- (IBAction)editingTextField:(id)sender;

- (IBAction)presentButton:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.questionTextField.delegate = self;
    self.questionTextField2.delegate =self;
    self.questionTextField3.delegate =self;

    self.questionTextField4.delegate =self;

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editingTextField:(id)sender {
}

- (IBAction)presentButton:(id)sender {
    
    OutputViewController *outputViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OVController"];
    outputViewController.delegate = self;
    outputViewController.dataSource = self;
    [self presentViewController :outputViewController animated:YES completion:^{
       
       
   }];


}

#pragma mark
-(NSString *)label{
    return self.questionTextField.text;
}
-(void)textfied:(NSString *)string
{
    self.inputLabel.text= string;
}
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField isEqual:self.questionTextField])
    {
        self.questionTextField.backgroundColor = UIColor.magentaColor;
       /* self.questionTextField2.backgroundColor = nil;
        self.questionTextField3.backgroundColor = nil;
        self.questionTextField4.backgroundColor = nil; */
    }
     else if ([textField isEqual:self.questionTextField2])
    {
        self.questionTextField2.backgroundColor = UIColor.yellowColor;
/* self.questionTextField.backgroundColor = nil;
        self.questionTextField3.backgroundColor = nil;
        self.questionTextField4.backgroundColor = nil; */   }
     else if ([textField isEqual:self.questionTextField3])
    {
        self.questionTextField3.backgroundColor = UIColor.redColor;
        /* .questionTextField2.backgroundColor = nil;
        self.questionTextField.backgroundColor = nil;
        self.questionTextField4.backgroundColor = nil; */
    }
    else if ([textField isEqual:self.questionTextField4])
    {
        self.questionTextField4.backgroundColor = UIColor.greenColor;
      /*   self.questionTextField2.backgroundColor = nil;
        self.questionTextField3.backgroundColor = nil;
        self.questionTextField.backgroundColor = nil; */
    }
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    textField.backgroundColor = UIColor.whiteColor;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (finalString.length == 4) {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}// called when 'return' key pressed. return NO to ignore.


@end
