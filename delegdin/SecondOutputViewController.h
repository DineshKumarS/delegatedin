//
//  SecondOutputViewController.h
//  delegdin
//
//  Created by APPLE on 26/01/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondOutputViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
