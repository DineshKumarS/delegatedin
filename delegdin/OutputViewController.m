//
//  OutputViewController.m
//  delegdin
//
//  Created by APPLE on 25/01/16.
//  Copyright © 2016 APPLE. All rights reserved.
//

#import "OutputViewController.h"
#import "SecondOutputViewController.h"

@interface OutputViewController ()
- (IBAction)resultButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *displayedQuestionLabel;
@property (weak, nonatomic) IBOutlet UITextField *InputAnswerTextField;
- (IBAction)secondButton:(id)sender;


@end

@implementation OutputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.displayedQuestionLabel.text = [self.dataSource label];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)resultButton:(id)sender {
    [self.delegate textfied:self.InputAnswerTextField.text];
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}
- (IBAction)secondButton:(id)sender {
   SecondOutputViewController *secondOutputViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SVO"];
    
    [self presentViewController:secondOutputViewController animated:YES completion:^{
        
        
    }];
}
@end
